import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import relativeTime from 'dayjs/plugin/relativeTime'
import { DayjsLocalizedFormat } from '@libs/dayjs/enums'
import { TimeRemainingOptions, TimeRemainingProps } from '@libs/dayjs/types'

dayjs.extend(duration)
dayjs.extend(relativeTime)

export const formatDate = (date: Date, format: string) => {
  return dayjs(date).format(format)
}

export const formatLocalDate = (date: Date, format: DayjsLocalizedFormat = DayjsLocalizedFormat.LLL) => {
  return dayjs(date).format(format)
}

export const isDatePassed = (date: string) => {
  return dayjs().isAfter(date)
}

const timeRemaining = (from: string, to: string, format: string = '%d days'): string => {
  const fromDayjs = dayjs(from)
  const toDayjs = dayjs(to)
  const diff = toDayjs.diff(fromDayjs, 'second')
  const duration = dayjs.duration(diff, 'second')
  return format
    .replace('%d', duration.days().toString())
    .replace('%h', duration.hours().toString())
    .replace('%m', duration.minutes().toString())
    .replace('%s', duration.seconds().toString())
}

export const getTimeRemaining = (data: TimeRemainingProps, options: TimeRemainingOptions) => {
  const { from, to } = data
  const { format } = options
  if (!from) return null
  if (!to) {
    if (isDatePassed(from)) {
      return timeRemaining(from, dayjs().format(), format)
    } else {
      return timeRemaining(dayjs().format(), from, format)
    }
  }
  return timeRemaining(from, to, format)
}
