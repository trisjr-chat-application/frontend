export enum USER_AND_ROOM_STATUS_ENUM {
  joined = 'JOINED',
  invited = 'INVITED',
}

export enum GET_USER_AND_ROOM_STATUS_ENUM {
  joined = 'JOINED',
  invited = 'INVITED',
  owner = 'OWNER',
}
