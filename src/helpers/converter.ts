export const convertText = (str: string): string => {
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
}

export const convertByteToMb = (bytes: number): number => {
  return Math.round(bytes / 1024 / 1024)
}
