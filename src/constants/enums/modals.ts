export enum MODALS_NAME_ENUM {
  CREATE_ROOM_MODAL = 'createRoomModal',
  RELATIONSHIP_MANAGEMENT_MODAL = 'relationshipManagementModal',
  ROOM_MANAGEMENT_MODAL = 'roomManagementModal',
  ADD_MEMBER_MODAL = 'addMemberModal',
}
