import { createI18n } from 'vue-i18n'

import en from './locales/en.json'
import vi from './locales/vi.json'
import { LANGUAGE } from '@constants/enums/store'

type MessageSchema = typeof en

const getLocale = () => {
  return localStorage.getItem('language') || LANGUAGE.EN
}

const i18n = createI18n<[MessageSchema], LANGUAGE.EN | LANGUAGE.VI>({
  locale: getLocale(),
  fallbackLocale: getLocale(),
  messages: {
    en,
    vi,
  },
})

export default i18n
