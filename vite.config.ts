import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': '/src',
      '@assets': '/src/assets',
      '@components': '/src/components',
      '@constants': '/src/constants',
      '@helpers': '/src/helpers',
      '@layouts': '/src/layouts',
      '@libs': '/src/libs',
      '@router': '/src/router',
      '@services': '/src/services',
      '@store': '/src/store',
      '@utils': '/src/utils',
      '@views': '/src/views',
    },
  },
})
