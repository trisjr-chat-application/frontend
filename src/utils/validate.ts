import type { Rule } from 'ant-design-vue/es/form'
import { VALIDATION_EMAIL, VALIDATION_NUMBER } from '@utils/validation'

export const VALIDATE_MESSAGES = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
  length: {
    range: '${label} must be between ${min} and ${max} characters',
    min: '${label} must be at least ${min} characters',
    max: '${label} must be at most ${max} characters',
  },
  confirm: {
    password: '${label} is not the same as password',
  },
}

export const VALIDATE = (_rule: Rule, value: string) => {
  const { required, type, min, max } = _rule
  if (required && !value) return Promise.reject(VALIDATE_MESSAGES.required)
  if (type && type === 'email' && !VALIDATION_EMAIL(value)) return Promise.reject(VALIDATE_MESSAGES.types.email)
  if (type && type === 'number' && !VALIDATION_NUMBER(value)) return Promise.reject(VALIDATE_MESSAGES.types.number)
  if (min && !max && value.length < min) return Promise.reject(VALIDATE_MESSAGES.length.min)
  if (!min && max && value.length > max) return Promise.reject(VALIDATE_MESSAGES.length.max)
  if (min && max && (value.length < min || value.length > max)) return Promise.reject(VALIDATE_MESSAGES.length.range)
  return Promise.resolve()
}
