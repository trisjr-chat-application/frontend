export enum ROOM_TYPE_ENUM {
  private = 'PRIVATE',
  group = 'GROUP',
}

export enum ROOM_KEY_VALIDITY {
  valid = 'valid',
  invalid = 'invalid',
  unknown = 'unknown',
}

export enum HANDLE_ROOM_ACTION_ENUM {
  accept = 'ACCEPT',
  reject = 'REJECT',
  add = 'ADD',
  remove = 'REMOVE',
  leave = 'LEAVE',
  delete = 'DELETE',
}
