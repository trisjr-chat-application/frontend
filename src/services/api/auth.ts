import { LOGIN_REQUEST, REGISTER_REQUEST } from '@constants/types/auth'
import AxiosClient, { apiPost } from '@services/client/AxiosClient'

export const apiLogin = (data: LOGIN_REQUEST) => {
  return apiPost('/auth/login', data)
}

export const apiRegister = (data: REGISTER_REQUEST) => {
  return apiPost('/auth/register', data)
}

export const apiGetProfile = () => {
  return AxiosClient.get('/auth/profile')
}
