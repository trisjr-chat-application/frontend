import { CREATE_ROOM_REQUEST, ROOM, ROOM_ACTION_TYPE } from '@constants/types/room'
import { Rule } from 'ant-design-vue/es/form'
import { VALIDATE } from '@utils/validate'
import { HANDLE_ROOM_ACTION_ENUM, ROOM_TYPE_ENUM } from '@constants/enums/room'
import { DEFAULT_MESSAGE } from '@constants/message'

export const DEFAULT_ROOM: ROOM = {
  roomKey: '',
  usersInvited: [],
  image: '',
  name: '',
  type: ROOM_TYPE_ENUM.private,
  createdAt: new Date(),
  id: 0,
  lastMessage: DEFAULT_MESSAGE,
  updatedAt: new Date(),
}

export const DEFAULT_CREATE_ROOM_REQUEST: CREATE_ROOM_REQUEST = {
  name: '',
  photo: '',
  members: [],
  key: '',
}

export const CREATE_ROOM_RULES: Record<string, Rule[]> = {
  name: [{ required: true, type: 'string', validator: VALIDATE, trigger: 'blur' }],
  photo: [{ type: 'string', validator: VALIDATE, trigger: 'blur' }],
  members: [{ required: true, type: 'array', validator: VALIDATE, trigger: 'blur' }],
}

export const ROOM_ACTIONS: Array<ROOM_ACTION_TYPE> = [
  {
    text: 'Accept',
    type: 'primary',
    action: HANDLE_ROOM_ACTION_ENUM.accept,
    title: 'Are you sure to accept this invitation?',
    description: 'You will be added to this room',
  },
  {
    text: 'Reject',
    type: 'danger',
    action: HANDLE_ROOM_ACTION_ENUM.reject,
    title: 'Are you sure to reject this invitation?',
    description: 'You will not be added to this room',
  },
  {
    text: 'Remove',
    type: 'danger',
    action: HANDLE_ROOM_ACTION_ENUM.delete,
    title: 'Are you sure to remove this room?',
    description: 'This room will be deleted',
  },
  {
    text: 'Leave',
    type: 'danger',
    action: HANDLE_ROOM_ACTION_ENUM.leave,
    title: 'Are you sure to leave this room?',
    description: 'You will be removed from this room',
  },
]
