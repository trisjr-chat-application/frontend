export enum USER_RELATIONSHIP_STATUS {
  friend = 'FRIEND',
  sent_request = 'SENT_REQUEST',
  received_request = 'RECEIVED_REQUEST',
  none = 'NONE',
}

export enum RELATIONSHIP_ACTION {
  add_friend = 'ADD_FRIEND',
  accept_request = 'ACCEPT_REQUEST',
  cancel_request = 'CANCEL_REQUEST',
  reject_request = 'REJECT_REQUEST',
  unfriend = 'UNFRIEND',
}
