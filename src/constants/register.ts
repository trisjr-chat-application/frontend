import { REGISTER_REQUEST } from '@constants/types/auth'
import { GENDER_ENUM } from '@constants/enums/auth'
import { Rule } from 'ant-design-vue/es/form'
import { VALIDATE } from '@utils/validate'

export const DEFAULT_REGISTER_REQUEST: REGISTER_REQUEST = {
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  birthDate: '',
  password: '',
  gender: GENDER_ENUM.MALE,
  profilePicture: '',
}

export const REGISTER_FORM_RULES: Record<string, Rule[]> = {
  email: [{ required: true, type: 'email', validator: VALIDATE, trigger: 'blur' }],
  password: [{ required: true, min: 5, validator: VALIDATE, trigger: 'blur' }],
  firstName: [{ required: true, min: 2, validator: VALIDATE, trigger: 'blur' }],
  lastName: [{ required: true, min: 2, validator: VALIDATE, trigger: 'blur' }],
  phoneNumber: [{ required: true, min: 2, validator: VALIDATE, trigger: 'blur' }],
  birthDate: [{ required: true, validator: VALIDATE, trigger: 'blur' }],
}

export const FORM_LAYOUT = {
  labelCol: {
    style: {
      display: 'none',
    },
  },
}
