import { RELATIONSHIP_ACTION } from '@constants/enums/relationship'

export interface ACTION_TYPE {
  text: string
  type: string
  action: RELATIONSHIP_ACTION
}

export interface SEND_RELATIONSHIP_REQUEST {
  userId: number
  type: RELATIONSHIP_ACTION
}
