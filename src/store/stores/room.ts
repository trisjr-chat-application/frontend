import { Module } from 'vuex'
import { ROOM_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import {
  CHECK_ROOM_KEY_REQUEST,
  CHECK_ROOM_KEY_RESPONSE,
  CREATE_ROOM_REQUEST,
  CREATE_ROOM_RESPONSE,
  GET_ROOM_DETAIL_REQUEST,
  GET_ROOM_DETAIL_RESPONSE,
  GET_ROOMS_REQUEST,
  GET_ROOMS_RESPONSE,
  GET_USERS_INVITE_REQUEST,
  GET_USERS_INVITE_RESPONSE,
  HANDLE_ROOM_ACTION,
  UPDATE_ROOM_DETAIL_REQUEST,
} from '@constants/types/room'
import {
  apiCheckKey,
  apiCreateRoom,
  apiGetRoomDetail,
  apiGetRooms,
  apiGetUsersInvite,
  apiHandleRoomAction,
  apiUpdateRoomDetail,
} from '@services/api/room'
import { AxiosResponse } from 'axios'
import { toastSuccess } from '@libs/toast-notification'
import { DEFAULT_RESPONSE } from '@constants/types/default'
import { DEFAULT_ROOM } from '@constants/room'

const room: Module<ROOM_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    roomKeyValid: false,
    isCreator: false,
    roomDetail: DEFAULT_ROOM,
    usersInvite: [],
    rooms: [],
    imageMessages: [],
  },
  getters: {
    ...DEFAULT_GETTERS,
    roomKeyValid: (state: ROOM_STATE_TYPE) => state.roomKeyValid,
    isCreator: (state: ROOM_STATE_TYPE) => state.isCreator,
    roomDetail: (state: ROOM_STATE_TYPE) => state.roomDetail,
    usersInvite: (state: ROOM_STATE_TYPE) => state.usersInvite,
    rooms: (state: ROOM_STATE_TYPE) => state.rooms,
    imageMessages: (state: ROOM_STATE_TYPE) => state.imageMessages,
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    CHECK_ROOM_KEY_SUCCESS: (state: ROOM_STATE_TYPE, payload: CHECK_ROOM_KEY_RESPONSE) => {
      state.roomKeyValid = payload.data.roomKeyValid
    },
    CREATE_ROOM_SUCCESS: (state: ROOM_STATE_TYPE, payload: CREATE_ROOM_RESPONSE) => {
      toastSuccess(payload.message)
    },
    GET_ROOM_DETAIL_SUCCESS: (state: ROOM_STATE_TYPE, payload: GET_ROOM_DETAIL_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.roomDetail = payload.data.room
      state.isCreator = payload.data.isCreator
      state.imageMessages = payload.data.imageMessages
    },
    UPDATE_ROOM_DETAIL_SUCCESS: (state: ROOM_STATE_TYPE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      toastSuccess('Update room detail successfully')
    },
    GET_USERS_INVITE_SUCCESS: (state: ROOM_STATE_TYPE, payload: GET_USERS_INVITE_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.usersInvite = payload.data.users
    },
    GET_ROOMS_SUCCESS: (state: ROOM_STATE_TYPE, payload: GET_ROOMS_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.rooms = payload.data.rooms
    },
  },
  actions: {
    async checkRoomKey({ commit }, payload: CHECK_ROOM_KEY_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<CHECK_ROOM_KEY_RESPONSE> = await apiCheckKey(payload)
        commit('DEFAULT_SUCCESS')
        commit('CHECK_ROOM_KEY_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async createRoom({ commit }, payload: CREATE_ROOM_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<CREATE_ROOM_RESPONSE> = await apiCreateRoom(payload)
        commit('DEFAULT_SUCCESS', response)
        commit('CREATE_ROOM_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async getRoomDetail({ commit }, payload: GET_ROOM_DETAIL_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<GET_ROOM_DETAIL_RESPONSE> = await apiGetRoomDetail(payload)
        commit('GET_ROOM_DETAIL_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async updateRoomDetail({ commit }, payload: UPDATE_ROOM_DETAIL_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<CREATE_ROOM_RESPONSE> = await apiUpdateRoomDetail(payload)
        commit('UPDATE_ROOM_DETAIL_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async getUsersInvite({ commit }, payload: GET_USERS_INVITE_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<GET_USERS_INVITE_RESPONSE> = await apiGetUsersInvite(payload)
        commit('GET_USERS_INVITE_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async handleRoomAction({ commit }, payload: HANDLE_ROOM_ACTION) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<DEFAULT_RESPONSE> = await apiHandleRoomAction(payload)
        commit('DEFAULT_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async getRooms({ commit }, payload: GET_ROOMS_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<GET_ROOMS_RESPONSE> = await apiGetRooms(payload)
        commit('GET_ROOMS_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
  },
}

export default room;
