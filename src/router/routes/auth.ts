import { RouteRecordRaw } from 'vue-router'
import { LAYOUTS_ENUM, THEMES_ENUM } from '@constants/enums/layout'

const auth: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    // components: {
    //   default: () => import('@views/Home.vue'),
    //   sidebar: () => import('@components/layouts/Sidebar.vue'),
    // },
    redirect: '/chat',
    meta: {
      layout: LAYOUTS_ENUM.horizontal,
      requireAuth: true,
      title: 'Home',
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@views/Login.vue'),
    meta: {
      layout: LAYOUTS_ENUM.full,
      requireAuth: false,
      title: 'Login',
      theme: THEMES_ENUM.dark,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@views/Register.vue'),
    meta: {
      layout: LAYOUTS_ENUM.full,
      requireAuth: false,
      title: 'Register',
      theme: THEMES_ENUM.dark,
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@views/NotFound.vue'),
    meta: {
      layout: LAYOUTS_ENUM.full,
      requireAuth: false,
      title: 'Not Found',
      theme: THEMES_ENUM.light,
    },
  },
]

export default auth
