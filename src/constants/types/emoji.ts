import { EMOJI_CATEGORY } from '@constants/enums/emoji'
import { EMOJI } from '@constants/emoji'

export interface EMOJIS_TYPE {
  [EMOJI_CATEGORY.FREQUENTLY_USED]: EMOJI[]
  [EMOJI_CATEGORY.PEOPLE]: EMOJI[]
  [EMOJI_CATEGORY.NATURE]: EMOJI[]
  [EMOJI_CATEGORY.PLACES]: EMOJI[]
  [EMOJI_CATEGORY.OBJECTS]: EMOJI[]
  [EMOJI_CATEGORY.SYMBOLS]: EMOJI[]
}

export interface EMOJI_CATEGORY_TYPE {
  text: string
  emoji: EMOJI
  type: EMOJI_CATEGORY
}
