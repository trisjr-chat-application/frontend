import { createStore } from 'vuex'

import app from '@store/stores/app'
import auth from '@store/stores/auth'
import user from '@store/stores/user'
import room from '@store/stores/room'
import message from '@store/stores/message'
import sidebar from '@store/stores/sidebar'
import modal from '@store/stores/modal'

const store = createStore({
  modules: {
    app,
    auth,
    user,
    room,
    message,
    sidebar,
    modal,
  },
  strict: true,
  devtools: true,
})

export default store
