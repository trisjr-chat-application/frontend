import { RouteRecordRaw } from 'vue-router'
import { LAYOUTS_ENUM } from '@constants/enums/layout'

const chat: Array<RouteRecordRaw> = [
  {
    path: '/chat',
    name: 'Chat',
    components: {
      default: () => import('@views/Welcome.vue'),
      sidebar: () => import('@components/layouts/ChatSidebar.vue'),
    },
    meta: {
      layout: LAYOUTS_ENUM.horizontal,
      requireAuth: true,
      title: 'Chat',
    },
  },
  {
    path: '/chat/:key',
    name: 'ChatDetail',
    components: {
      default: () => import('@views/Chat.vue'),
      sidebar: () => import('@components/layouts/ChatSidebar.vue'),
    },
    meta: {
      layout: LAYOUTS_ENUM.horizontal,
      requireAuth: true,
      title: 'Chat',
    },
  },
]

export default chat
