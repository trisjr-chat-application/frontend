import { GET_CHAT_DETAIL_REQUEST, GET_CHAT_LIST_REQUEST, GET_MESSAGES_REQUEST } from '@constants/types/message'
import AxiosClient, { apiPost } from '@services/client/AxiosClient'

export const apiGetChatList = async (params: GET_CHAT_LIST_REQUEST) => {
  return AxiosClient.get('/message/chat-list', { params })
}

export const apiGetChatDetail = async (params: GET_CHAT_DETAIL_REQUEST) => {
  return AxiosClient.get('/message/chat-detail', { params })
}

export const apiGetMessages = async (params: GET_MESSAGES_REQUEST) => {
  return apiPost('/message/messages', params)
}
