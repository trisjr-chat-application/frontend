import { createRouter, createWebHistory } from 'vue-router'
import auth from '@router/routes/auth'
import chat from '@router/routes/chat'
import ChangePageTitle from '@helpers/change-page-title'
import store from '@/store'

const router = createRouter({
  history: createWebHistory(),
  routes: [...auth, ...chat],
  scrollBehavior: () => ({ left: 0, top: 0 }),
})

router.beforeEach((to, from, next) => {
  const { meta, name } = to
  if ((name === 'Login' || name === 'Register') && !!localStorage.getItem('token')) {
    next({ name: 'Home' })
  }
  if (meta) {
    const { title, requireAuth } = meta
    if (!requireAuth) {
      ChangePageTitle(title as string)
      next()
    } else {
      if (!localStorage.getItem('token')) {
        ChangePageTitle('Login')
        next({ name: 'Login' })
      } else {
        if (!store.getters['auth/isAuthenticated']) {
          store.dispatch('auth/getProfile').then(() => {
            if (store.getters['auth/isAuthenticated']) {
              store.dispatch('message/connectWebsocket').then(() => {})
              ChangePageTitle(title as string)
              next()
            } else {
              store.dispatch('auth/logout').then(() => {
                ChangePageTitle('Login')
                next({ name: 'Login' })
              })
            }
          })
        } else {
          ChangePageTitle(title as string)
          next()
        }
      }
    }
  } else {
    ChangePageTitle('Not Found')
    next({
      name: 'NotFound',
    })
  }
})

export default router
