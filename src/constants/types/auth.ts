import { GENDER_ENUM, RELATIONSHIP_ENUM } from '@constants/enums/auth'
import { DEFAULT_RESPONSE } from '@constants/types/default'
import { USER } from '@constants/types/user'

export const DEFAULT_USER: USER = {
  id: 0,
  firstName: '',
  lastName: '',
  email: '',
  phoneNumber: '',
  birthDate: '',
  address: '',
  city: '',
  country: '',
  profilePicture: '',
  coverPicture: '',
  aboutMe: '',
  gender: GENDER_ENUM.MALE,
  relationshipStatus: RELATIONSHIP_ENUM.SINGLE,
}

export interface LOGIN_REQUEST {
  email: string
  password: string
  remember: boolean
}

export interface LOGIN_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    token: string
    user: USER
  }
}

export interface REGISTER_REQUEST {
  firstName: string
  lastName: string
  email: string
  phoneNumber: string
  birthDate: string
  password: string
  profilePicture: string
  gender: GENDER_ENUM
}

export interface GET_PROFILE_RESPONSE extends DEFAULT_RESPONSE {
  data: USER
}
