import { USER_RELATIONSHIP_STATUS } from '@constants/enums/relationship'
import { DEFAULT_RESPONSE } from '@constants/types/default'
import { GENDER_ENUM, RELATIONSHIP_ENUM } from '@constants/enums/auth'

export interface GET_USER_REQUEST {
  key: string
  relationshipStatus: USER_RELATIONSHIP_STATUS
}

export interface USER {
  id: number
  firstName: string
  lastName: string
  email: string
  phoneNumber: string
  birthDate: string
  address: string
  city: string
  country: string
  profilePicture: string
  coverPicture: string
  aboutMe: string
  gender: GENDER_ENUM
  relationshipStatus: RELATIONSHIP_ENUM
}

export interface GET_USER_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    users: Array<USER>
  }
}
