import Stomp from 'stompjs'
import SockJS from 'sockjs-client/dist/sockjs'
import {
  MESSAGE,
  MESSAGE_RECEIVED_TYPE,
  READ_MESSAGE_TYPE,
  SEND_MESSAGE_REQUEST,
  SEND_REACTION_MESSAGE_REQUEST,
  UPDATE_CHAT_LIST_TYPE,
} from '@constants/types/message'
import store from '@/store'
import { toastError, toastInfo, toastSuccess, toastWarning } from '@libs/toast-notification'
import { CHANGE_LAST_SEEN_TYPE, NOTIFICATION_TYPE } from '@constants/types/socket'
import { NOTIFICATION_STATUS_ENUM } from '@constants/enums/socket'
import { MESSAGE_TYPE_ENUM } from '@constants/enums/message'

const url = import.meta.env.VITE_APP_API_SERVER

export const handleConnectWebsocket = () => {
  const sockJS = new SockJS(url)
  const stompClient = Stomp.over(sockJS)
  stompClient.debug = () => {}

  stompClient.connect(
    {
      Authorization: 'Bearer ' + localStorage.getItem('token'),
    },
    handleConnect,
    onError
  )

  function handleConnect() {
    stompClient.subscribe(`/user/${store.getters['auth/user'].id}/queue/messages`, (message) => onShowMessage(JSON.parse(message.body)))
    stompClient.subscribe(`/user/${store.getters['auth/user'].id}/queue/reactions`, (message) => onChangeReaction(JSON.parse(message.body)))
    stompClient.subscribe(`/user/${store.getters['auth/user'].id}/queue/notifications`, (message) => onNotification(JSON.parse(message.body)))
    stompClient.subscribe(`/user/${store.getters['auth/user'].id}/queue/read`, (message) => onChangeLastSeen(JSON.parse(message.body)))
  }

  const handleRead = (request: READ_MESSAGE_TYPE) => {
    stompClient.send(
      '/app/read',
      {
        Authorization: 'Bearer ' + store.getters['auth/token'],
      },
      JSON.stringify(request)
    )
  }

  function onError(error: any) {
    console.log('error', error)
  }

  const onShowMessage = (payload: MESSAGE_RECEIVED_TYPE) => {
    const room = payload.room
    const currentRoomKey = store.getters['message/chatDetail'].roomKey
    const message = payload.message
    const isSeen: boolean = room.roomKey === currentRoomKey
    if (room.roomKey !== currentRoomKey) {
      if (message.type !== MESSAGE_TYPE_ENUM.notification) {
        toastSuccess(message.sender.lastName + ' sent you a message')
      }
    } else {
      store.dispatch('message/addMessage', message).then(() => {})
      const readMessage: READ_MESSAGE_TYPE = {
        roomKey: room.roomKey,
      }
      handleRead(readMessage)
    }
    const updateChatList: UPDATE_CHAT_LIST_TYPE = {
      key: room.roomKey,
      message: message,
      isSeen,
    }
    store.dispatch('sidebar/updateChatList', updateChatList).then(() => {})
  }

  const onChangeReaction = (payload: MESSAGE) => {
    store.dispatch('message/updateMessage', payload).then(() => {})
  }

  function sendMessage(messageRequest: SEND_MESSAGE_REQUEST) {
    stompClient.send(
      '/app/send',
      {
        Authorization: 'Bearer ' + store.getters['auth/token'],
      },
      JSON.stringify(messageRequest)
    )
  }

  const onNotification = (notification: NOTIFICATION_TYPE) => {
    switch (notification.status) {
      case NOTIFICATION_STATUS_ENUM.success:
        toastSuccess(notification.content)
        break
      case NOTIFICATION_STATUS_ENUM.error:
        toastError(notification.content)
        break
      case NOTIFICATION_STATUS_ENUM.info:
        toastInfo(notification.content)
        break
      case NOTIFICATION_STATUS_ENUM.warning:
        toastWarning(notification.content)
        break

      default:
        break
    }
  }

  const onChangeLastSeen = (payload: CHANGE_LAST_SEEN_TYPE) => {
    store.dispatch('message/updateSeenMessage', payload).then(() => {})
  }

  function disconnect() {
    return stompClient.disconnect(() => {
      console.log('disconnected')
    })
  }

  function reactionMessage(request: SEND_REACTION_MESSAGE_REQUEST) {
    stompClient.send(
      '/app/reaction',
      {
        Authorization: 'Bearer ' + store.getters['auth/token'],
      },
      JSON.stringify(request)
    )
  }

  return {
    sendMessage,
    disconnect,
    reactionMessage,
    handleRead,
  }
}
