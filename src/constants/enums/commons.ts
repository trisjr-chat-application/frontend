export enum PASSWORD_STRENGTH {
  WEAK = 'weak',
  MEDIUM = 'medium',
  STRONG = 'strong',
}

export enum NOTIFICATION_TYPE {
  SUCCESS = 'success',
  ERROR = 'error',
  INFO = 'info',
  WARNING = 'warning',
}

export enum BUTTON_TYPE {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  TERTIARY = 'tertiary',
  DANGER = 'danger',
  WARNING = 'warning',
  SUCCESS = 'success',
  INFO = 'info',
  LINK = 'link',
}
