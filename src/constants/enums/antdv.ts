export enum UPLOAD_PHOTO_LIST_TYPE {
  TEXT = 'text',
  PICTURE = 'picture',
  PICTURE_CARD = 'picture-card',
}
