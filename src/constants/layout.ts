import LayoutHorizontal from '@layouts/LayoutHorizontal.vue'
import LayoutFull from '@layouts/LayoutFull.vue'
import { LAYOUTS_ENUM } from '@constants/enums/layout'

export const LAYOUTS = {
  [LAYOUTS_ENUM.horizontal]: LayoutHorizontal,
  [LAYOUTS_ENUM.full]: LayoutFull,
}
