export interface DEFAULT_RESPONSE {
  status: boolean
  message: string
  data: any
}
