export enum LAYOUTS_ENUM {
  horizontal = 'horizontal',
  full = 'full',
}

export enum THEMES_ENUM {
  light = 'light',
  dark = 'dark',
}
