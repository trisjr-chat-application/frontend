import { Module } from 'vuex'
import { MODALS_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import { MODALS_NAME_ENUM } from '@constants/enums/modals'

const modal: Module<MODALS_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    [MODALS_NAME_ENUM.CREATE_ROOM_MODAL]: false,
    [MODALS_NAME_ENUM.RELATIONSHIP_MANAGEMENT_MODAL]: false,
    [MODALS_NAME_ENUM.ROOM_MANAGEMENT_MODAL]: false,
    [MODALS_NAME_ENUM.ADD_MEMBER_MODAL]: false,
  },
  getters: {
    ...DEFAULT_GETTERS,
    [MODALS_NAME_ENUM.CREATE_ROOM_MODAL]: (state: MODALS_STATE_TYPE) => state[MODALS_NAME_ENUM.CREATE_ROOM_MODAL],
    [MODALS_NAME_ENUM.RELATIONSHIP_MANAGEMENT_MODAL]: (state: MODALS_STATE_TYPE) => state[MODALS_NAME_ENUM.RELATIONSHIP_MANAGEMENT_MODAL],
    [MODALS_NAME_ENUM.ROOM_MANAGEMENT_MODAL]: (state: MODALS_STATE_TYPE) => state[MODALS_NAME_ENUM.ROOM_MANAGEMENT_MODAL],
    [MODALS_NAME_ENUM.ADD_MEMBER_MODAL]: (state: MODALS_STATE_TYPE) => state[MODALS_NAME_ENUM.ADD_MEMBER_MODAL],
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    OPEN_MODAL: function (state: MODALS_STATE_TYPE, payload: MODALS_NAME_ENUM) {
      state[payload] = true
    },
    CLOSE_MODAL: function (state: MODALS_STATE_TYPE, payload: MODALS_NAME_ENUM) {
      state[payload] = false
    },
  },
  actions: {
    openModal: function ({ commit, dispatch }, payload: MODALS_NAME_ENUM) {
      commit('OPEN_MODAL', payload)
      // Close other modal
      Object.values(MODALS_NAME_ENUM).forEach((modalName: MODALS_NAME_ENUM) => {
        if (modalName !== payload) {
          commit('CLOSE_MODAL', modalName)
        }
      })
    },
    closeModal: function ({ commit }, payload: MODALS_NAME_ENUM) {
      commit('CLOSE_MODAL', payload)
    },
  },
}

export default modal
