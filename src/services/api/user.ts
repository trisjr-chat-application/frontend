import { GET_USER_REQUEST } from '@constants/types/user'
import AxiosClient, { apiPost } from '@services/client/AxiosClient'
import { SEND_RELATIONSHIP_REQUEST } from '@constants/types/relationship'

export const apiGetUser = async (params: GET_USER_REQUEST) => {
  return AxiosClient.get('/user/get-users', { params: params })
}

export const apiSendRelationship = async (params: SEND_RELATIONSHIP_REQUEST) => {
  return apiPost('/user/send-request', params)
}
