import { createApp } from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import Antd from 'ant-design-vue'
import i18n from '@/i18n'
import PerfectScrollbar from 'vue3-perfect-scrollbar'
import vue3GoogleLogin from 'vue3-google-login'

import './style.css'
import 'ant-design-vue/dist/antd.css'
import '@assets/global-styles.scss'
import 'vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css'

const app = createApp(App)

app.use(router)
app.use(store)
app.use(Antd)
app.use(i18n)
app.use(PerfectScrollbar, {
  watchOptions: true,
  options: {
    wheelSpeed: 0.5,
    wheelPropagation: true,
    minScrollbarLength: 20,
  },
})
app.use(vue3GoogleLogin, {
  clientId: '928250014183-avgpk2f01216ap1o15odn9ncia081iiq.apps.googleusercontent.com',
})

app.mount('#app')
