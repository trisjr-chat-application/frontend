import { SUFFIX_TITLE } from '@constants/common'

const ChangePageTitle = (title: string = '') => {
  document.title = title ? `${title} - ${SUFFIX_TITLE}` : SUFFIX_TITLE
}

export default ChangePageTitle
