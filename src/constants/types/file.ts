import { DEFAULT_RESPONSE } from '@constants/types/default'

export interface FILE {
  id: number
  name: string
  path: string
  type: string
  url: string
  size: number
  limitTime: Date
  createdAt: Date
  updatedAt: Date
}

export interface FILE_DATA {
  fileName: string
  filePath: string
  fileType: string
  fileUrl: string
}

export interface FILE_UPLOAD_RESPONSE extends DEFAULT_RESPONSE {
  data: FILE_DATA
}
