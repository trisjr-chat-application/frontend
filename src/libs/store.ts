import { STATE_TYPE } from '@constants/types/store'
import { toastError } from '@libs/toast-notification'
import { DEFAULT_RESPONSE } from '@constants/types/default'

export const DEFAULT_STATE: STATE_TYPE = {
  status: false,
  message: '',
  loading: false,
}

export const DEFAULT_GETTERS = {
  status: (state: STATE_TYPE) => state.status,
  loading: (state: STATE_TYPE) => state.loading,
}

export const DEFAULT_MUTATIONS = {
  DEFAULT_REQUEST: (state: STATE_TYPE) => {
    REQUEST(state)
  },
  DEFAULT_SUCCESS: (state: STATE_TYPE) => {
    SUCCESS(state)
  },
  DEFAULT_FAILURE: (state: STATE_TYPE, { message }: DEFAULT_RESPONSE) => {
    toastError(message)
    FAILURE(state)
  },
}

export const REQUEST = (state: STATE_TYPE) => {
  state.loading = true
  state.message = ''
}

export const SUCCESS = (state: STATE_TYPE) => {
  state.status = true
  state.loading = false
}

export const FAILURE = (state: STATE_TYPE) => {
  state.status = false
  state.loading = false
}
