import { MESSAGE_REACTION_ENUM, MESSAGE_TYPE_ENUM } from '@constants/enums/message'
import MessageNotification from '@components/messages/MessageNotification.vue'
import MessageNormal from '@components/messages/MessageNormal.vue'
import { MESSAGE, MESSAGE_REACTION } from '@constants/types/message'
import MessageImage from '@components/messages/MessageImage.vue'
import MessageFile from '@components/messages/MessageFile.vue'
import { DEFAULT_USER } from '@constants/types/auth'

export const DEFAULT_MESSAGE: MESSAGE = {
  id: 0,
  type: MESSAGE_TYPE_ENUM.text,
  content: '',
  sender: DEFAULT_USER,
  roomKey: '',
  replyMessage: undefined,
  messageStatuses: [],
  createdAt: new Date(),
  updatedAt: new Date(),
  urlPreview: undefined,
}

export const MESSAGE_COMPONENTS = {
  [MESSAGE_TYPE_ENUM.notification]: MessageNotification,
  [MESSAGE_TYPE_ENUM.text]: MessageNormal,
  [MESSAGE_TYPE_ENUM.image]: MessageImage,
  [MESSAGE_TYPE_ENUM.file]: MessageFile,
}

export const MESSAGE_REACTIONS: Array<MESSAGE_REACTION> = [
  {
    emoji: '😂',
    title: 'Haha',
    action: MESSAGE_REACTION_ENUM.haha,
    count: 0,
  },
  {
    emoji: '👍',
    title: 'Like',
    action: MESSAGE_REACTION_ENUM.like,
    count: 0,
  },
  {
    emoji: '❤️',
    title: 'Love',
    action: MESSAGE_REACTION_ENUM.love,
    count: 0,
  },
  {
    emoji: '😢',
    title: 'Sad',
    action: MESSAGE_REACTION_ENUM.sad,
    count: 0,
  },
  {
    emoji: '😮',
    title: 'Wow',
    action: MESSAGE_REACTION_ENUM.wow,
    count: 0,
  },
  {
    emoji: '😡',
    title: 'Angry',
    action: MESSAGE_REACTION_ENUM.angry,
    count: 0,
  },
]
