import { Module } from 'vuex'
import { AUTH_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import { DEFAULT_USER, GET_PROFILE_RESPONSE, LOGIN_REQUEST, LOGIN_RESPONSE, REGISTER_REQUEST } from '@constants/types/auth'
import { apiGetProfile, apiLogin, apiRegister } from '@services/api/auth'
import { AxiosResponse } from 'axios'
import { toastSuccess } from '@libs/toast-notification'
import { DEFAULT_RESPONSE } from '@constants/types/default'

const tokenKey = 'token'
const userKey = 'USER'

const auth: Module<AUTH_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    token: localStorage.getItem(tokenKey) || '',
    isAuthenticated: false,
    user: DEFAULT_USER,
  },
  getters: {
    ...DEFAULT_GETTERS,
    token: (state: AUTH_STATE_TYPE) => state.token,
    isAuthenticated: (state: AUTH_STATE_TYPE) => state.isAuthenticated,
    user: (state: AUTH_STATE_TYPE) => state.user,
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    LOGIN_REQUEST_SUCCESS: (state: AUTH_STATE_TYPE, payload: LOGIN_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      const {
        data: { token, user },
        message,
      } = payload
      toastSuccess(message)
      localStorage.setItem(tokenKey, token)
      localStorage.setItem(userKey, JSON.stringify(user))
      state.token = token
      state.isAuthenticated = true
      state.user = user
    },
    LOGOUT_REQUEST_SUCCESS: (state: AUTH_STATE_TYPE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      localStorage.removeItem(tokenKey)
      localStorage.removeItem(userKey)
      state.token = ''
      state.isAuthenticated = false
      state.user = DEFAULT_USER
    },
    REGISTER_REQUEST_SUCCESS: (state: AUTH_STATE_TYPE, payload: DEFAULT_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      const { message } = payload
      toastSuccess(message)
    },
    GET_PROFILE_REQUEST_SUCCESS: (state: AUTH_STATE_TYPE, payload: GET_PROFILE_RESPONSE) => {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      const { data: user } = payload
      state.user = user
      state.isAuthenticated = true
    },
  },
  actions: {
    login: async ({ commit }, params: LOGIN_REQUEST) => {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<LOGIN_RESPONSE> = await apiLogin(params)
        commit('LOGIN_REQUEST_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    logout: async ({ commit }) => {
      commit('DEFAULT_REQUEST')
      try {
        commit('LOGOUT_REQUEST_SUCCESS')
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    register: async ({ commit }, params: REGISTER_REQUEST) => {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<Response> = await apiRegister(params)
        commit('REGISTER_REQUEST_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    getProfile: async ({ commit }) => {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<GET_PROFILE_RESPONSE> = await apiGetProfile()
        commit('GET_PROFILE_REQUEST_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
  },
}

export default auth
