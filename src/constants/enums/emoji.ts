export enum EMOJI_CATEGORY {
  FREQUENTLY_USED = 'Frequently Used',
  PEOPLE = 'People',
  NATURE = 'Nature',
  PLACES = 'Places',
  OBJECTS = 'Objects',
  SYMBOLS = 'Symbols',
}
