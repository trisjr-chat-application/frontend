import { NOTIFICATION_STATUS_ENUM } from '@constants/enums/socket'
import { USER_AND_ROOM } from '@constants/types/userAndRoom'

export interface NOTIFICATION_TYPE {
  content: string
  status: NOTIFICATION_STATUS_ENUM
  roomKey: string
}

export interface CHANGE_LAST_SEEN_TYPE {
  roomKey: string
  users: Array<USER_AND_ROOM>
}
