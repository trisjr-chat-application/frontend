export enum MESSAGE_TYPE_ENUM {
  text = 'TEXT',
  image = 'IMAGE',
  video = 'VIDEO',
  file = 'FILE',
  notification = 'NOTIFICATION',
}

export enum MESSAGE_REACTION_ENUM {
  none = 'NONE',
  like = 'LIKE',
  haha = 'HAHA',
  wow = 'WOW',
  sad = 'SAD',
  angry = 'ANGRY',
  love = 'LOVE',
}

export enum MESSAGE_VARIANT_ENUM {
  single = 'SINGLE',
  first = 'FIRST',
  middle = 'MIDDLE',
  last = 'LAST',
}

export enum MESSAGE_GET_ENUM {
  sender = 'SENDER',
  recipient = 'RECIPIENT',
}
