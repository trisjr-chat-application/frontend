import { USER_AND_ROOM_STATUS_ENUM } from '@constants/enums/userAndRoom'
import { USER } from '@constants/types/user'

export interface USER_AND_ROOM {
  id: number
  user: USER
  isCreator: boolean
  status: USER_AND_ROOM_STATUS_ENUM
  lastSeenAt: Date
  createdAt: Date
  updatedAt: Date
}
