export interface TimeRemainingType {
  days: number
  hours: number
  minutes: number
  seconds: number
}

export interface TimeRemainingProps {
  from: string
  to?: string
}

export interface TimeRemainingOptions {
  format: string
}
