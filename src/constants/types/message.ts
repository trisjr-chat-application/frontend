import { DEFAULT_RESPONSE } from '@constants/types/default'
import { USER } from '@constants/types/user'
import { ROOM } from '@constants/types/room'
import { MESSAGE_REACTION_ENUM, MESSAGE_TYPE_ENUM } from '@constants/enums/message'
import { FILE } from '@constants/types/file'
import { USER_AND_ROOM } from '@constants/types/userAndRoom'

export interface MESSAGE {
  id: number
  content: string
  sender: USER
  roomKey: string
  type: MESSAGE_TYPE_ENUM
  replyMessage?: MESSAGE
  messageStatuses: MESSAGE_STATUS[]
  createdAt: Date
  updatedAt: Date
  urlPreview?: URL_PREVIEW
  file?: FILE
}

export interface MESSAGE_STATUS {
  id: number
  message: MESSAGE
  user: USER
  reaction: MESSAGE_REACTION_ENUM
  createdAt: Date
  updatedAt: Date
}

export interface CHAT_ITEM {
  key: string
  photo: string
  name: string
  lastMessage: MESSAGE
  messageStatus: MESSAGE_STATUS
  isSeen: boolean
}

export interface GET_CHAT_LIST_REQUEST {
  key: string
}

export interface GET_CHAT_LIST_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    chatList: CHAT_ITEM[]
  }
}

export interface GET_CHAT_DETAIL_REQUEST {
  key: string
}

export interface GET_CHAT_DETAIL_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    chatDetail: ROOM
    messages: MESSAGE[]
    users: Array<USER_AND_ROOM>
  }
}

export interface GET_MESSAGES_REQUEST {
  key: string
  page: number
  perPage: number
}

export interface GET_MESSAGES_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    messages: MESSAGE[]
  }
}

export interface SEND_MESSAGE_REQUEST {
  key: string
  content: string
  type: MESSAGE_TYPE_ENUM
  replyMessageId?: Number
}

export interface MESSAGE_REACTION {
  emoji: string
  title: string
  action: MESSAGE_REACTION_ENUM
  count: number
}

export interface SEND_REACTION_MESSAGE_REQUEST {
  messageId: Number
  reaction: MESSAGE_REACTION_ENUM
}

export interface URL_PREVIEW {
  title: string
  description: string
  image: string
  url: string
}

export interface UPDATE_CHAT_LIST_TYPE {
  key: string
  message: MESSAGE
  isSeen: boolean
}

export interface MESSAGE_RECEIVED_TYPE {
  message: MESSAGE
  room: ROOM
}

export interface READ_MESSAGE_TYPE {
  roomKey: string
}
