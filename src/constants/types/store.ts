import { USER } from '@constants/types/user'
import { CHAT_ITEM, MESSAGE } from '@constants/types/message'
import { ROOM } from '@constants/types/room'
import { MODALS_NAME_ENUM } from '@constants/enums/modals'
import { ROOM_TYPE_ENUM } from '@constants/enums/room'
import { USER_AND_ROOM } from '@constants/types/userAndRoom'

export interface STATE_TYPE {
  status: Boolean
  message: String
  loading: Boolean
}

export interface APP_STATE_TYPE {
  language: String
  isSidebarOpen: Boolean
}

export interface AUTH_STATE_TYPE extends STATE_TYPE {
  token: String
  isAuthenticated: Boolean
  user: USER
}

export interface USER_STATE_TYPE extends STATE_TYPE {
  friends: Array<USER>
  sentRequests: Array<USER>
  receivedRequests: Array<USER>
  users: Array<USER>
}

export interface ROOM_STATE_TYPE extends STATE_TYPE {
  roomKeyValid: Boolean
  roomDetail: ROOM
  isCreator: Boolean
  usersInvite: Array<USER>
  rooms: Array<ROOM>
  imageMessages: Array<MESSAGE>
}

export interface SIDE_BAR_STATE_TYPE extends STATE_TYPE {
  chatList: Array<CHAT_ITEM>
}

export interface CHAT_STATE_TYPE extends STATE_TYPE {
  chatDetail: ROOM
  type: ROOM_TYPE_ENUM
  messages: Array<MESSAGE>
  socketServer: any
  users: Array<USER_AND_ROOM>
}

export interface MODALS_STATE_TYPE extends STATE_TYPE {
  [MODALS_NAME_ENUM.CREATE_ROOM_MODAL]: Boolean
  [MODALS_NAME_ENUM.RELATIONSHIP_MANAGEMENT_MODAL]: Boolean
  [MODALS_NAME_ENUM.ROOM_MANAGEMENT_MODAL]: Boolean
  [MODALS_NAME_ENUM.ADD_MEMBER_MODAL]: Boolean
}
