export enum NOTIFICATION_STATUS_ENUM {
  success = 'SUCCESS',
  error = 'ERROR',
  info = 'INFO',
  warning = 'WARNING',
}
