import { Module } from 'vuex'
import { APP_STATE_TYPE } from '@constants/types/store'
import { LANGUAGE } from '@constants/enums/store'
import i18n from '@/i18n'

const language = localStorage.getItem('language') || LANGUAGE.EN

const app: Module<APP_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    language,
    isSidebarOpen: true,
  },
  getters: {
    language: (state) => state.language,
    isSidebarOpen: (state) => state.isSidebarOpen,
  },
  mutations: {
    SET_LANGUAGE: (state: APP_STATE_TYPE, language: LANGUAGE) => {
      state.language = language
      localStorage.setItem('language', language)
      console.log('SET_LANGUAGE', language)
      i18n.global.locale = language
    },
    TOGGLE_SIDEBAR: (state: APP_STATE_TYPE) => {
      state.isSidebarOpen = !state.isSidebarOpen
    },
  },
  actions: {
    setLanguage: ({ commit }, language) => {
      commit('SET_LANGUAGE', language)
    },
    toggleSidebar: ({ commit }) => {
      commit('TOGGLE_SIDEBAR')
    },
  },
}

export default app
