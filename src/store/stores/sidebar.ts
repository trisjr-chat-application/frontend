import { Module } from 'vuex'
import { SIDE_BAR_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import { CHAT_ITEM, GET_CHAT_LIST_REQUEST, GET_CHAT_LIST_RESPONSE, UPDATE_CHAT_LIST_TYPE } from '@constants/types/message'
import { AxiosResponse } from 'axios'
import { apiGetChatList } from '@services/api/message'

const sidebar: Module<SIDE_BAR_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    chatList: [],
  },
  getters: {
    ...DEFAULT_GETTERS,
    chatList: (state: SIDE_BAR_STATE_TYPE) => state.chatList,
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    GET_CHAT_LIST_SUCCESS(state: SIDE_BAR_STATE_TYPE, payload: GET_CHAT_LIST_RESPONSE) {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.chatList = payload.data.chatList
    },
    UPDATE_CHAT_LIST(state: SIDE_BAR_STATE_TYPE, { message, key }: UPDATE_CHAT_LIST_TYPE) {
      const index = state.chatList.findIndex((chat: CHAT_ITEM) => chat.key === key)
      if (index !== -1) {
        state.chatList[index].lastMessage = message
        const item = state.chatList[index]
        state.chatList.splice(index, 1)
        state.chatList.unshift(item)
      }
    },
    UPDATE_SEEN_CHAT_LIST(state: SIDE_BAR_STATE_TYPE, payload: { key: string; isSeen: boolean }) {
      const index = state.chatList.findIndex((chat: CHAT_ITEM) => chat.key === payload.key)
      if (index !== -1) {
        state.chatList[index].isSeen = payload.isSeen
      }
    },
  },
  actions: {
    getChatList: async function ({ commit }, payload: GET_CHAT_LIST_REQUEST = { key: '' }) {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<GET_CHAT_LIST_RESPONSE> = await apiGetChatList(payload)
        commit('GET_CHAT_LIST_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    updateChatList: function ({ commit }, payload: UPDATE_CHAT_LIST_TYPE) {
      commit('UPDATE_CHAT_LIST', payload)
      commit('UPDATE_SEEN_CHAT_LIST', { key: payload.key, isSeen: payload.isSeen })
    },
    updateSeenMessage: function ({ commit }, payload: { key: string; isSeen: boolean }) {
      commit('UPDATE_SEEN_CHAT_LIST', payload)
    },
  },
}

export default sidebar
