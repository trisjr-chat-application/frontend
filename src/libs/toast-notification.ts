import { NOTIFICATION_TYPE } from '@constants/enums/commons'
import { notification } from 'ant-design-vue'
import { CheckCircleTwoTone, CloseCircleTwoTone, ExclamationCircleTwoTone, InfoCircleTwoTone } from '@ant-design/icons-vue'

notification.config({
  placement: 'topRight',
  duration: 4,
})

const NotificationIcon = {
  [NOTIFICATION_TYPE.SUCCESS]: {
    icon: CheckCircleTwoTone,
    class: 'toast-success',
  },
  [NOTIFICATION_TYPE.ERROR]: {
    icon: CloseCircleTwoTone,
    class: 'toast-error',
  },
  [NOTIFICATION_TYPE.INFO]: {
    icon: InfoCircleTwoTone,
    class: 'toast-info',
  },
  [NOTIFICATION_TYPE.WARNING]: {
    icon: ExclamationCircleTwoTone,
    class: 'toast-warning',
  },
}

const toastNotification = (type: NOTIFICATION_TYPE, message: string) => {
  notification[type]({
    message,
    class: NotificationIcon[type].class,
  })
}

export const toastSuccess = (message: string) => {
  toastNotification(NOTIFICATION_TYPE.SUCCESS, message)
}

export const toastError = (message: string = 'Something was error...') => {
  toastNotification(NOTIFICATION_TYPE.ERROR, message)
}

export const toastInfo = (message: string) => {
  toastNotification(NOTIFICATION_TYPE.INFO, message)
}

export const toastWarning = (message: string) => {
  toastNotification(NOTIFICATION_TYPE.WARNING, message)
}
