import { Module } from 'vuex'
import { USER_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import { GET_USER_REQUEST, GET_USER_RESPONSE } from '@constants/types/user'
import { apiGetUser, apiSendRelationship } from '@services/api/user'
import { AxiosResponse } from 'axios'
import { USER_RELATIONSHIP_STATUS } from '@constants/enums/relationship'
import { SEND_RELATIONSHIP_REQUEST } from '@constants/types/relationship'
import { DEFAULT_RESPONSE } from '@constants/types/default'

const user: Module<USER_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    friends: [],
    sentRequests: [],
    receivedRequests: [],
    users: [],
  },
  getters: {
    ...DEFAULT_GETTERS,
    users: (state: USER_STATE_TYPE) => state.users,
    friends: (state: USER_STATE_TYPE) => state.friends,
    sentRequests: (state: USER_STATE_TYPE) => state.sentRequests,
    receivedRequests: (state: USER_STATE_TYPE) => state.receivedRequests,
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    GET_USERS_REQUEST_SUCCESS: (state: USER_STATE_TYPE, payload: GET_USER_RESPONSE) => {
      state.users = payload.data.users
    },
    GET_FRIENDS_REQUEST_SUCCESS: (state: USER_STATE_TYPE, payload: GET_USER_RESPONSE) => {
      state.friends = payload.data.users
    },
    GET_SENT_REQUESTS_REQUEST_SUCCESS: (state: USER_STATE_TYPE, payload: GET_USER_RESPONSE) => {
      state.sentRequests = payload.data.users
    },
    GET_RECEIVED_REQUESTS_REQUEST_SUCCESS: (state: USER_STATE_TYPE, payload: GET_USER_RESPONSE) => {
      state.receivedRequests = payload.data.users
    },
  },
  actions: {
    async getUsers({ commit }, payload: GET_USER_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<GET_USER_RESPONSE> = await apiGetUser(payload)
        commit('DEFAULT_SUCCESS')
        if (payload.relationshipStatus === USER_RELATIONSHIP_STATUS.friend) {
          commit('GET_FRIENDS_REQUEST_SUCCESS', response)
        }
        if (payload.relationshipStatus === USER_RELATIONSHIP_STATUS.sent_request) {
          commit('GET_SENT_REQUESTS_REQUEST_SUCCESS', response)
        }
        if (payload.relationshipStatus === USER_RELATIONSHIP_STATUS.received_request) {
          commit('GET_RECEIVED_REQUESTS_REQUEST_SUCCESS', response)
        }
        if (payload.relationshipStatus === USER_RELATIONSHIP_STATUS.none) {
          commit('GET_USERS_REQUEST_SUCCESS', response)
        }
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    async sendRelationshipRequest({ commit }, payload: SEND_RELATIONSHIP_REQUEST) {
      try {
        commit('DEFAULT_REQUEST')
        const response: AxiosResponse<DEFAULT_RESPONSE> = await apiSendRelationship(payload)
        commit('DEFAULT_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
  },
}

export default user
