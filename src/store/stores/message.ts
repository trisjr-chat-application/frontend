import { Module } from 'vuex'
import { CHAT_STATE_TYPE } from '@constants/types/store'
import { DEFAULT_GETTERS, DEFAULT_MUTATIONS, DEFAULT_STATE } from '@libs/store'
import {
  GET_CHAT_DETAIL_REQUEST,
  GET_CHAT_DETAIL_RESPONSE,
  GET_MESSAGES_REQUEST,
  GET_MESSAGES_RESPONSE,
  MESSAGE,
  SEND_MESSAGE_REQUEST,
  SEND_REACTION_MESSAGE_REQUEST,
} from '@constants/types/message'
import { AxiosResponse } from 'axios'
import { apiGetChatDetail, apiGetMessages } from '@services/api/message'
import { handleConnectWebsocket } from '@helpers/socket'
import { DEFAULT_ROOM } from '@constants/room'
import { ROOM_TYPE_ENUM } from '@constants/enums/room'
import { CHANGE_LAST_SEEN_TYPE } from '@constants/types/socket'

const message: Module<CHAT_STATE_TYPE, any> = {
  namespaced: true,
  state: {
    ...DEFAULT_STATE,
    loading: true,
    chatDetail: DEFAULT_ROOM,
    messages: [],
    socketServer: {
      disconnect: () => {},
      sendMessage: () => {},
      reactionMessage: () => {},
      handleRead: () => {},
    },
    type: ROOM_TYPE_ENUM.group,
    users: [],
  },
  getters: {
    ...DEFAULT_GETTERS,
    chatDetail: (state: CHAT_STATE_TYPE) => state.chatDetail,
    type: (state: CHAT_STATE_TYPE) => state.type,
    messages: (state: CHAT_STATE_TYPE) => state.messages,
    socketServer: (state: CHAT_STATE_TYPE) => state.socketServer,
    users: (state: CHAT_STATE_TYPE) => state.users,
  },
  mutations: {
    ...DEFAULT_MUTATIONS,
    GET_CHAT_DETAIL_SUCCESS(state: CHAT_STATE_TYPE, payload: GET_CHAT_DETAIL_RESPONSE) {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.chatDetail = payload.data.chatDetail
      state.type = payload.data.chatDetail.type
      state.messages = payload.data.messages
      state.users = payload.data.users
    },
    GET_MESSAGES_SUCCESS(state: CHAT_STATE_TYPE, payload: GET_MESSAGES_RESPONSE) {
      DEFAULT_MUTATIONS.DEFAULT_SUCCESS(state)
      state.messages = payload.data.messages
    },

    CONNECT_WEBSOCKET_SUCCESS(state: CHAT_STATE_TYPE, payload: Object) {
      state.socketServer = payload
    },
    DISCONNECT_WEBSOCKET_SUCCESS(state: CHAT_STATE_TYPE) {
      state.socketServer = {}
    },
    ADD_MESSAGE(state: CHAT_STATE_TYPE, payload: MESSAGE) {
      state.messages.push(payload)
    },
    UPDATE_MESSAGE(state: CHAT_STATE_TYPE, payload: MESSAGE) {
      const index = state.messages.findIndex((message: MESSAGE) => message.id === payload.id)
      if (index !== -1) {
        state.messages[index] = payload
      }
    },
    CLEAR_MESSAGES(state: CHAT_STATE_TYPE) {
      state.messages = []
    },
    UPDATE_SEEN_MESSAGE(state: CHAT_STATE_TYPE, payload: CHANGE_LAST_SEEN_TYPE) {
      const { roomKey, users } = payload
      if (state.chatDetail.roomKey === roomKey) {
        state.users = users
      }
    },
  },
  actions: {
    getChatDetail: async function ({ commit, dispatch, state }, payload: GET_CHAT_DETAIL_REQUEST) {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<GET_CHAT_DETAIL_RESPONSE> = await apiGetChatDetail(payload)
        commit('GET_CHAT_DETAIL_SUCCESS', response)
        await dispatch('sidebar/updateSeenMessage', { key: payload.key, isSeen: true }, { root: true })
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    getMessages: async function ({ commit }, payload: GET_MESSAGES_REQUEST) {
      commit('DEFAULT_REQUEST')
      try {
        const response: AxiosResponse<GET_MESSAGES_RESPONSE> = await apiGetMessages(payload)
        commit('GET_MESSAGES_SUCCESS', response)
      } catch ({ data }) {
        commit('DEFAULT_FAILURE', data)
      }
    },
    connectWebsocket: function ({ commit }) {
      const socketJs = handleConnectWebsocket()
      commit('CONNECT_WEBSOCKET_SUCCESS', socketJs)
    },
    disconnectWebsocket: function ({ commit, state }) {
      state.socketServer.disconnect()
      commit('DISCONNECT_WEBSOCKET_SUCCESS')
    },
    sendMessage: function ({ commit, state }, payload: SEND_MESSAGE_REQUEST) {
      state.socketServer.sendMessage(payload)
    },
    reactionMessage: function ({ commit, state }, payload: SEND_REACTION_MESSAGE_REQUEST) {
      state.socketServer.reactionMessage(payload)
    },
    addMessage: function ({ commit }, payload: MESSAGE) {
      commit('ADD_MESSAGE', payload)
    },
    updateMessage: function ({ commit }, payload: MESSAGE) {
      commit('UPDATE_MESSAGE', payload)
    },
    updateSeenMessage: function ({ commit, dispatch, state }, payload: CHANGE_LAST_SEEN_TYPE) {
      commit('UPDATE_SEEN_MESSAGE', payload)
    },
  },
}

export default message
