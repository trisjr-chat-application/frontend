import { DEFAULT_RESPONSE } from '@constants/types/default'
import { USER } from '@constants/types/user'
import { HANDLE_ROOM_ACTION_ENUM, ROOM_TYPE_ENUM } from '@constants/enums/room'
import { GET_USER_AND_ROOM_STATUS_ENUM } from '@constants/enums/userAndRoom'
import { MESSAGE } from '@constants/types/message'
import { USER_AND_ROOM } from '@constants/types/userAndRoom'

export interface ROOM {
  id: number
  roomKey: string
  name: string
  image: string
  type: ROOM_TYPE_ENUM
  lastMessage: MESSAGE
  usersInvited: USER_AND_ROOM[]
  createdAt: Date
  updatedAt: Date
}

export interface CREATE_ROOM_REQUEST {
  name: string
  key: string
  photo: string
  members: number[]
}

export interface CHECK_ROOM_KEY_REQUEST {
  key: string
}

export interface CHECK_ROOM_KEY_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    roomKeyValid: boolean
  }
}

export interface CREATE_ROOM_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    room: ROOM
  }
}

export interface GET_ROOM_DETAIL_REQUEST {
  key: string
}

export interface GET_ROOM_DETAIL_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    room: ROOM
    isCreator: boolean
    imageMessages: Array<MESSAGE>
  }
}

export interface ROOM_ACTION_TYPE {
  text: string
  type: 'primary' | 'danger' | 'default' | 'ghost' | 'link' | 'dashed' | 'text'
  action: HANDLE_ROOM_ACTION_ENUM
  title: string
  description: string
}

export interface HANDLE_ROOM_ACTION {
  key: string
  action: HANDLE_ROOM_ACTION_ENUM
  usersId?: Array<number>
}

export interface UPDATE_ROOM_DETAIL_REQUEST {
  key: string
  name: string
  image: string
}

export interface GET_USERS_INVITE_REQUEST {
  key: string
  search: string
}

export interface GET_USERS_INVITE_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    users: USER[]
  }
}

export interface GET_ROOMS_REQUEST {
  search: string
  status: GET_USER_AND_ROOM_STATUS_ENUM
}

export interface GET_ROOMS_RESPONSE extends DEFAULT_RESPONSE {
  data: {
    rooms: ROOM[]
  }
}
