import axios from 'axios'
import { eraseCookie, setCookie } from '@utils/cookie'
import router from '@/router'

const AxiosClient = axios.create({
  baseURL: import.meta.env.VITE_APP_API_DEV,
  headers: {
    'content-type': 'application/json',
  },
})

AxiosClient.interceptors.request.use((config) => {
  const token = localStorage.getItem('token')
  if (token) {
    setCookie('token', token, 0)
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${token}`,
    }
  }
  return config
})

AxiosClient.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error?.response?.status === 401) {
      // log out
      localStorage.clear()
      eraseCookie('token')
      router.push('/login').then(() => {})
    }
    return Promise.reject(error.response)
  }
)

const listCancels: any = {}

const checkCancel = (url: string, isCancel: boolean) => isCancel && listCancels[url] && listCancels[url]()
const getParamCancel = (url: string, isCancel: boolean) => {
  if (isCancel) {
    return {
      cancelToken: new axios.CancelToken((cancelToken) => {
        listCancels[url] = cancelToken
      }),
    }
  }
  return {}
}

export const apiGet = (url: string, params = {}, isCancel = true) => {
  try {
    checkCancel(url, isCancel)
    return AxiosClient.get(url, { params, ...getParamCancel(url, isCancel) })
  } catch (e) {
    console.log('ERROR: ', e)
  }
}

export const apiPost = (url: string, data = {}) => {
  return AxiosClient.post(url, data)
}

export default AxiosClient
