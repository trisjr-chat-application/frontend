import {
  CHECK_ROOM_KEY_REQUEST,
  CREATE_ROOM_REQUEST,
  GET_ROOM_DETAIL_REQUEST,
  GET_ROOMS_REQUEST,
  GET_USERS_INVITE_REQUEST,
  HANDLE_ROOM_ACTION,
  UPDATE_ROOM_DETAIL_REQUEST,
} from '@constants/types/room'
import AxiosClient, { apiPost } from '@services/client/AxiosClient'

export const apiCheckKey = async (params: CHECK_ROOM_KEY_REQUEST) => {
  return AxiosClient.get('/room/check-key', { params: params })
}

export const apiCreateRoom = async (params: CREATE_ROOM_REQUEST) => {
  return apiPost('/room/create-room', params)
}

export const apiGetRoomDetail = async (params: GET_ROOM_DETAIL_REQUEST) => {
  return AxiosClient.get('/room/get-room-detail', { params: params })
}

export const apiHandleRoomAction = async (params: HANDLE_ROOM_ACTION) => {
  return apiPost('/room/handle-room-action', params)
}

export const apiUpdateRoomDetail = async (params: UPDATE_ROOM_DETAIL_REQUEST) => {
  return apiPost('/room/update-room-detail', params)
}

export const apiGetUsersInvite = async (params: GET_USERS_INVITE_REQUEST) => {
  return AxiosClient.get('/room/get-users-invite', { params: params })
}

export const apiGetRooms = async (params: GET_ROOMS_REQUEST) => {
  return AxiosClient.get('/room/get-rooms', { params })
}
