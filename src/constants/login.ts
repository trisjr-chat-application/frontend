import { LOGIN_REQUEST } from '@constants/types/auth'
import { Rule } from 'ant-design-vue/es/form'
import { VALIDATE } from '@utils/validate'

export const DEFAULT_LOGIN_REQUEST: LOGIN_REQUEST = {
  email: '',
  password: '',
  remember: false,
}

export const LOGIN_FORM_RULES: Record<string, Rule[]> = {
  email: [{ required: true, type: 'email', validator: VALIDATE, trigger: 'blur' }],
  password: [{ required: true, min: 5, validator: VALIDATE, trigger: 'blur' }],
}

export const FORM_LAYOUT = {
  labelCol: {
    style: {
      display: 'none',
    },
  },
}
